﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> a = new List<int> { 5, 6, 7 };
            List<int> b = new List<int> { 3, 6, 10 };

            int uno = 0;
            int dos = 0;

            for (int i = 0; i <= 2; i++)
            {
                if (a[i] > b[i])
                {
                    uno = uno+1;
                }
                else if(a[i] < b[i])
                {
                    dos = dos+1;
                }
            }

            List<int> result = new List<int> { uno, dos };

            Console.WriteLine(String.Join(" ", result));
            Console.Read();
        }
    }
}
